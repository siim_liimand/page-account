"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountPage = void 0;
const preact_1 = require("preact");
const button_1 = require("@dixid/components/dist/buttons/button");
const AccountPage = ({ pageData: { title } }) => {
    return ((0, preact_1.h)("div", null,
        (0, preact_1.h)("h1", null, title),
        (0, preact_1.h)(button_1.Button, { children: "test", href: "https://www.google.com" }),
        (0, preact_1.h)(button_1.Button, { children: (0, preact_1.h)("span", null, "Tere"), onClick: () => {
                alert('test');
            } }),
        (0, preact_1.h)(button_1.Button, { href: "https://www.neti.ee" }, "neti.ee"),
        (0, preact_1.h)("img", { src: "/images/suvine_ah.jpg", width: 750, height: 1138 })));
};
exports.AccountPage = AccountPage;
//# sourceMappingURL=index.js.map