import { h } from 'preact';
import { PageComponent } from '@dixid/front-router/dist/interfaces';
import { Button } from '@dixid/components/dist/buttons/button';

export const AccountPage: PageComponent = ({ pageData: { title } }) => {
  return (
    <div>
      <h1>{title}</h1>
      <Button children="test" href="https://www.google.com" />
      <Button
        children={<span>Tere</span>}
        onClick={() => {
          alert('test');
        }}
      />
      <Button href="https://www.neti.ee">neti.ee</Button>
      <img src="/images/suvine_ah.jpg" width={750} height={1138} />
    </div>
  );
};
